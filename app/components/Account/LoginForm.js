import React, { useState, useRef } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";
import { size, isEmpty } from "lodash";
import Loading from "../Loading";
import * as firebase from "firebase";
import { validateEmail } from "../../utils/validation";
import { useNavigation } from "@react-navigation/native";

const LoginForm = ({ toastRef }) => {
  const [showPassword, setShowPassword] = useState(false);
  const [formData, setFormData] = useState(defaultFormValue());
  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  const onSubmit = () => {
    const { email, password } = formData;
    if (isEmpty(email) || isEmpty(password)) {
      toastRef.current.show("Todos os campos são obrigatórios.");
    } else if (!validateEmail(email)) {
      toastRef.current.show("O Email está inválido");
    } else if (size(password) < 6) {
      toastRef.current.show("A senha tem que ter pelo menos 6 caracteres.");
    } else {
      setLoading(true);
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          setLoading(false);
          navigation.navigate("account");
        })
        .catch(() => {
          setLoading(false);
          toastRef.current.show("E-mail ou senha incorreta.");
        });
    }
  };

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.nativeEvent.text });
  };

  return (
    <View style={StyleSheet.formContainer}>
      <Input
        placeholder="E-mail"
        containerStyle={styles.inputForm}
        onChange={(e) => onChange(e, "email")}
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Senha"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={!showPassword}
        onChange={(e) => onChange(e, "password")}
        rightIcon={
          <Icon
            type="material-community"
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setShowPassword(!showPassword)}
          />
        }
      />
      <Button
        title="Fazer Login"
        containerStyle={styles.btnContainerLogin}
        buttonStyle={styles.btnLogin}
        onPress={onSubmit}
      />
      <Loading isVisible={loading} text="Fazendo login..." />
    </View>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  inputForm: {
    width: "100%",
    marginTop: 20,
  },
  btnContainerLogin: {
    marginTop: 20,
    width: "95%",
  },
  btnLogin: {
    backgroundColor: "#f67f00",
  },
  iconRight: {
    color: "#c1c1c1",
  },
});

const defaultFormValue = () => {
  return {
    email: "",
    password: "",
  };
};

export default LoginForm;
