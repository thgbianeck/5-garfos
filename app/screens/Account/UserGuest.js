import React from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import { Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

const UserGuest = () => {
  const navigation = useNavigation();
  return (
    <ScrollView centerContent={true} style={styles.viewBody}>
      <Image
        style={styles.image}
        source={require("../../../assets/img/user-guest.jpg")}
        resizeMode="contain"
      />
      <Text style={styles.title}>Verifique o seu perfil em 5-Garfos</Text>
      <Text style={styles.description}>
        Como descreveria o melhor restaurante? Busque e visualize os melhores
        restaurantes de uma forma fácil, vote em quais gostar mais e comente
        como foi a sua experiência.
      </Text>
      <View style={styles.viewBtn}>
        <Button
          buttonStyle={styles.btnStyle}
          containerStyle={styles.btnContainer}
          title="Veja seu Perfil"
          onPress={() => navigation.navigate("login")}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  viewBody: {
    marginLeft: 30,
    marginRight: 30,
  },
  image: {
    height: 300,
    width: "100%",
    marginBottom: 40,
  },
  title: {
    fontWeight: "bold",
    fontSize: 19,
    marginBottom: 10,
    textAlign: "center",
  },
  description: {
    textAlign: "center",
    marginBottom: 20,
  },
  viewBtn: {
    flex: 1,
    alignItems: "center",
  },
  btnStyle: {
    backgroundColor: "#f67f00",
  },
  btnContainer: {
    width: "70%",
  },
});

export default UserGuest;
